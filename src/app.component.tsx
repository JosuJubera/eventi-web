import axios from "axios";
import Cookies from "js-cookie";
import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "typeface-roboto";

import { EditEventComponent } from "./ui/views/edit-event";
import { EventDetailsComponent } from "./ui/views/event-details";
import { EventsComponent } from "./ui/views/events";
import { HeaderComponent } from "./ui/components/header";
import { LandingComponent } from "./ui/views/landing";
import { NewEventComponent } from "./ui/views/new-event";
import { NewMeetComponent } from "./ui/views/new-meet";
import { NotFoundComponent } from "./ui/views/not-found";
import { ProfileComponentWithRouter } from "./ui/views/profile/profile.component";
import { SignUpComponent } from "./ui/views/sign-up";
import { useSelector, useDispatch } from "react-redux";
import { EditMeetComponent } from "./ui/views/edit-meet";
import { setToken, setUser } from "./state/actions";
import { fetchUser } from "./core/services/user/user";

export const AppComponent = (): JSX.Element => {
  const dispatch = useDispatch();
  let token = useSelector((state: any) => state.token);

  if (!token) {
    token = localStorage.getItem("token");
    if (token) {
      dispatch(setToken(token));
    }
  }
  if (token) {
    axios.interceptors.request.use((config: any) => {
      config.headers.Authorization = token;
      return config;
    });
  }

  useEffect(() => {
    const t = Cookies.get("x-auth-cookie") || token;
    if (t) {
      localStorage.setItem("token", t);
      dispatch(setToken(t));
      Cookies.remove("x-auth-cookie");
    }
    if (token) {
      fetchUser().then((u) => dispatch(setUser(u.data)));
    }
  }, [dispatch, token]);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <Router>
        {token ? <HeaderComponent /> : ""}
        <div style={{ flexGrow: 1, display: "flex" }}>
          <Switch>
            <Route exact path="/">
              {token ? <EventsComponent /> : <LandingComponent />}
            </Route>

            <Route path="/sign-up">
              <SignUpComponent />
            </Route>

            <Route path="/profile">
              <ProfileComponentWithRouter token={token} />
            </Route>

            <Route path="/event/new">
              <NewEventComponent />
            </Route>

            <Route path="/event/:eventId/meet/new">
              <NewMeetComponent />
            </Route>

            <Route path="/event/:eventId/meet/:id/edit">
              <EditMeetComponent />
            </Route>

            <Route path="/event/:eventId/edit">
              <EditEventComponent />
            </Route>

            <Route path="/event/:eventId">
              <EventDetailsComponent />
            </Route>

            <Route path="*">
              <NotFoundComponent />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
};
