import { IUser, User } from "./user";

export interface IAuthorizedUser {
  token: string;
  user: IUser;
}

export class AuthorizedUser {
  token: string;
  user: User;

  constructor(ob: IAuthorizedUser) {
    this.token = ob.token;
    this.user = new User(ob.user);
  }
}
