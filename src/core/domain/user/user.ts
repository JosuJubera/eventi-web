import Moment from "moment";
import { Credentials } from "../credentials/credentials";

export interface IUser {
  birthday?: number;
  country?: string;
  email?: string;
  _id?: string;
  name?: string;
  password?: string;
  surname?: string;
}

export class User implements IUser {
  birthday?: number;
  country?: string;
  email?: string;
  _id?: string;
  name?: string;
  password?: string;
  surname?: string;

  constructor(ob?: IUser) {
    if (ob && ob.email && ob.password) {
      const { email, password } = new Credentials(ob.email, ob.password);
      this.birthday = Moment(ob.birthday).valueOf();
      this.country = ob.country;
      this.email = email;
      this._id = ob._id;
      this.name = ob.name;
      this.password = password;
      this.surname = ob.surname;
    }
  }
}
