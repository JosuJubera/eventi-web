import { RequiredField } from "./required-field";

export class Email extends RequiredField {
  constructor(field: string) {
    super(field);
    this.error = this.checkEmail(field);
  }

  private checkEmail(field: string): string | undefined {
    const err = this.checkField(field);
    if (err) {
      return err;
    }
    if (!field.match(pattern)) {
      return emailError;
    }
  }
}

const emailError = "Invalid email address";
const pattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
