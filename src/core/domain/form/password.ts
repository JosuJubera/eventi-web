import { RequiredField } from "./required-field";

export class Password extends RequiredField {
  constructor(field: string) {
    super(field);
    this.error = this.checkPassword(field);
  }

  equalityError(field: string): string | undefined {
    return this.equalField(field) ? "" : passwordErrorOptions[4];
  }

  private checkPassword(field: string): string | undefined {
    const err = this.checkField(field);
    if (err) {
      return err;
    }
    if (!field.match(patternDigit)) {
      return passwordErrorOptions[1];
    }
    if (!field.match(patternSymbol)) {
      return passwordErrorOptions[1];
    }
    if (!field.match(patternLength)) {
      return passwordErrorOptions[2];
    }
    if (!field.match(patternLower)) {
      return passwordErrorOptions[3];
    }
    if (!field.match(patternUpper)) {
      return passwordErrorOptions[3];
    }
  }
}

const passwordErrorOptions = {
  1: "Password must include at least one number and symbol",
  2: "Password must be at least 8 characters long and a maximum of 16",
  3: "Password must include both lower and upper case characters",
  4: "Password and confirmation must be the same"
};

const patternDigit = /^(?=.*\d)/;
const patternLength = /^(?=.{8,16}$)/;
const patternLower = /^(?=.*[a-z])/;
const patternSymbol = /^(?=.*[!@#$%^&])/;
const patternUpper = /^(?=.*[A-Z])/;
