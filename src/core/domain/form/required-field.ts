export class RequiredField {
  field: string;
  error: string | undefined;

  constructor(field: string) {
    this.field = field;
    this.error = this.checkField(field);
  }

  equalityError(field: string): string | undefined {
    return this.equalField(field) ? "" : errorOptions[1];
  }

  protected checkField(field: string): string | undefined {
    if (!field) {
      return errorOptions[0];
    }
  }

  protected equalField(field: string): boolean {
    return this.field === field;
  }
}

const errorOptions = {
  0: "Required",
  1: "Fields don no match"
};
