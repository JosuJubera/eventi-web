import Moment from "moment";

import { Meet } from "../meet/meet";
import { User } from "../user/user";

export interface IEvent {
  _id: string;
  city: string;
  description: string;
  endDate: number;
  location?: string;
  logo?: string;
  meets: Meet[];
  name: string;
  owner: User;
  password?: string;
  startDate: number;
}

export class Event implements IEvent {
  _id: string;
  city: string;
  description: string;
  endDate: number;
  location?: string;
  logo?: string;
  meets: Meet[];
  name: string;
  owner: User;
  password?: string;
  startDate: number;

  constructor(ob: IEvent) {
    this._id = ob._id;
    this.city = ob.city;
    this.description = ob.description;
    this.endDate = Moment(ob.endDate).valueOf();
    this.location = ob.location;
    this.logo = ob.logo;
    this.meets = ob.meets;
    this.name = ob.name;
    this.owner = ob.owner;
    this.password = ob.password;
    this.startDate = Moment(ob.startDate).valueOf();
  }
}
