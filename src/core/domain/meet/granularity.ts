export type Granularity = Minute | Hour | Day;
export class Day {
  id: TDay = "days";
  factor: number = 86400000;
}

export class Hour {
  id: THour = "hours";
  factor: number = 3600000;
}
export class Minute {
  id: TMinute = "minutes";
  factor: number = 60000;
}
export type TDay = "days";
export type THour = "hours";
export type TMinute = "minutes";

export const granularities: Array<Granularity> = [
  new Minute(),
  new Hour(),
  new Day(),
];
