import Moment from "moment";

export interface IMeet {
  assistants: number;
  assistantsLimit: number;
  date: number;
  description: string;
  duration: number;
  granularity?: number;
  _id: string;
  location: string;
  logo: string;
  name: string;
  speaker: string;
  speakerLogo: string;
}

export class Meet implements IMeet {
  assistants: number;
  assistantsLimit: number;
  date: number;
  description: string;
  duration: number;
  _id: string;
  location: string;
  logo: string;
  name: string;
  speaker: string;
  speakerLogo: string;

  constructor(ob: IMeet) {
    this.assistants = ob.assistants;
    this.assistantsLimit = ob.assistantsLimit;
    this.date = Moment(ob.date).valueOf();
    this.description = ob.description;
    this.duration = ob.duration * (ob.granularity || 1);
    this._id = ob._id;
    this.location = ob.location;
    this.logo = ob.logo;
    this.name = ob.name;
    this.speaker = ob.speaker;
    this.speakerLogo = ob.speakerLogo;
  }
}
