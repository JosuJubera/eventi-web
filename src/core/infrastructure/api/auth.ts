import axios, { AxiosResponse } from "axios";

import { Credentials } from "../../domain/credentials/credentials";
import { IAuthorizedUser } from "../../domain/user/authorizedUser";
import { User } from "../../domain/user/user";

export const postSignin = (
  credentials: Credentials
): Promise<AxiosResponse<IAuthorizedUser>> => {
  const url = "http://35.224.198.148:3001/api/signin/local";

  return axios.post(url, credentials);
};

export const postSignup = (
  user: User
): Promise<AxiosResponse<IAuthorizedUser>> => {
  const url = "http://35.224.198.148:3001/api/signup/local";

  return axios.post(url, user);
};
