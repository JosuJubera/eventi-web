import axios, { AxiosResponse } from "axios";

const postMeet = (meet: any): Promise<AxiosResponse<any>> => {
  const url = `http://35.224.198.148:3001/api/v1/meets/`;

  return axios.post(url, meet);
};

export { postMeet };
