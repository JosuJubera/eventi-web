import axios, { AxiosResponse } from "axios";
import { Meet } from "../../domain/meet/meet";

const postMeet = (id: any): Promise<AxiosResponse<Meet>> => {
  const url = `http://35.224.198.148:3001/api/v1/meet/${id}/assist`;

  return axios.post(url);
};

const patchMeet = (meet: any): Promise<AxiosResponse<any>> => {
  const url = `http://35.224.198.148:3001/api/v1/meet/${meet._id}`;

  return axios.patch(url, meet);
};

const deleteMeet = (id: any): Promise<AxiosResponse<Meet>> => {
  const url = `http://35.224.198.148:3001/api/v1/meet/${id}`;

  return axios.delete(url);
};

export { deleteMeet, patchMeet, postMeet };
