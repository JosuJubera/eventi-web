import axios, { AxiosResponse } from "axios";

import { Event } from "../../domain/event/event";

export interface GetEventsParams {
  city?: string;
  id?: string;
  favorites?: string;
  owner?: string;
}

const getEvents = (
  startDate: number,
  endDate: number,
  params?: GetEventsParams
): Promise<AxiosResponse<Event[]>> => {
  const url = `http://35.224.198.148:3001/api/v1/events/${startDate}/${endDate}`;

  return axios.get(url, { params });
};

const postEvent = (event: Event): Promise<AxiosResponse<Event>> => {
  const url = `http://35.224.198.148:3001/api/v1/events`;

  return axios.post(url, event);
};

export { getEvents, postEvent };
