import axios, { AxiosResponse } from "axios";

import { Event } from "../../domain/event/event";

const getEvent = (id: string): Promise<AxiosResponse<Event>> => {
  const url = `http://35.224.198.148:3001/api/v1/event/${id}`;

  return axios.get(url);
};

const postEvent = (id: any): Promise<AxiosResponse<Event>> => {
  const url = `http://35.224.198.148:3001/api/v1/event/${id}/favorite`;

  return axios.post(url);
};

const patchEvent = (event: any): Promise<AxiosResponse<Event>> => {
  const url = `http://35.224.198.148:3001/api/v1/event/${event._id}`;

  return axios.patch(url, event);
};

const deleteEvent = (id: any): Promise<AxiosResponse<Event>> => {
  const url = `http://35.224.198.148:3001/api/v1/event/${id}`;

  return axios.delete(url);
};

export { deleteEvent, getEvent, patchEvent, postEvent };
