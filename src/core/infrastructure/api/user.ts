import axios, { AxiosResponse } from "axios";

import { User } from "../../domain/user/user";

const getUser = (): Promise<AxiosResponse<User>> => {
  const url = `http://35.224.198.148:3001/api/v1/user`;

  return axios.get(url);
};

const postReauth = (ob: any): Promise<AxiosResponse<User>> => {
  const url = `http://35.224.198.148:3001/api/v1/user`;

  return axios.post(url, ob);
};

const putUser = (ob: any): Promise<AxiosResponse<User>> => {
  const url = `http://35.224.198.148:3001/api/v1/user/${ob._id}`;

  return axios.put(url, ob);
};

export { getUser, postReauth, putUser };
