import { AxiosResponse } from "axios";

import { postMeet } from "../../infrastructure/api/meets";

export const createMeet = (meet: any): Promise<AxiosResponse<any>> =>
  postMeet(meet);
