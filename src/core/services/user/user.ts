import { AxiosResponse } from "axios";

import { User } from "../../domain/user/user";
import { getUser, postReauth, putUser } from "../../infrastructure/api/user";
import { Credentials } from "../../domain/credentials/credentials";

export const fetchUser = (): Promise<AxiosResponse<User>> => getUser();

export const reauth = (ob: any): Promise<AxiosResponse<any>> => {
  const credentials = new Credentials(ob.email, ob.password);
  return postReauth(credentials);
};

export const updateUser = (ob: any): Promise<AxiosResponse<any>> => {
  return putUser(ob);
};
