import { createContext } from "react";

import { User } from "../../domain/user/user";

export const UserContext = createContext({
  user: undefined,
  setUser: (_user: User) => {},
});
