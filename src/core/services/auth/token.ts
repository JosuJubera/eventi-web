import { createContext } from "react";

export const TokenContext = createContext({
  token: localStorage.getItem("token") || "",
  setToken: (_token: string) => {}
});
