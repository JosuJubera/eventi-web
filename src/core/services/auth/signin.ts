import { AxiosResponse } from "axios";

import { Credentials } from "../../domain/credentials/credentials";
import { IAuthorizedUser } from "../../domain/user/authorizedUser";
import { postSignin } from "../../infrastructure/api/auth";

export const signin = (
  email: string,
  password: any
): Promise<AxiosResponse<IAuthorizedUser>> => {
  return postSignin(new Credentials(email, password));
};
