import { AxiosResponse } from "axios";

import { IAuthorizedUser } from "../../domain/user/authorizedUser";
import { postSignup } from "../../infrastructure/api/auth";

export const signup = (user: any): Promise<AxiosResponse<IAuthorizedUser>> => {
  return postSignup(user);
};
