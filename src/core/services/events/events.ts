import { AxiosResponse } from "axios";

import { Event } from "../../domain/event/event";
import {
  getEvents,
  GetEventsParams,
  postEvent,
} from "../../infrastructure/api/events";

export const fetchEvents = (
  startDate: number,
  endDate: number,
  params?: GetEventsParams
): Promise<AxiosResponse<Event[]>> => {
  const p = {
    ...(params?.city && { city: params.city }),
    ...(params?.id && { id: params.id }),
    ...(params?.favorites && { favorites: params.favorites }),
    ...(params?.owner && { owner: params.owner }),
  };
  return getEvents(startDate, endDate, p);
};

export const createEvent = (event: Event): Promise<AxiosResponse<Event>> =>
  postEvent(event);
