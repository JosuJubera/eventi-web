import { AxiosResponse } from "axios";

import { Event } from "../../domain/event/event";
import {
  deleteEvent,
  getEvent,
  patchEvent,
  postEvent,
} from "../../infrastructure/api/event";

export const favoriteEvent = (id: any): Promise<AxiosResponse<any>> =>
  postEvent(id);

export const fetchEvent = (id: string): Promise<AxiosResponse<Event>> =>
  getEvent(id);

export const updateEvent = (event: any): Promise<AxiosResponse<any>> =>
  patchEvent(event);

export const removeEvent = (id: any): Promise<AxiosResponse<any>> =>
  deleteEvent(id);
