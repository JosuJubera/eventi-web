import { AxiosResponse } from "axios";

import { deleteMeet, patchMeet, postMeet } from "../../infrastructure/api/meet";

export const assistMeet = (id: any): Promise<AxiosResponse<any>> =>
  postMeet(id);

export const updateMeet = (meet: any): Promise<AxiosResponse<any>> =>
  patchMeet(meet);

export const removeMeet = (id: any): Promise<AxiosResponse<any>> =>
  deleteMeet(id);
