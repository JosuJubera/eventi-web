import {
  SELECT_MEET,
  SelectMeetActionTypes,
} from "../actions/selectMeet.action";

const initialState: any = {};

export const selectMeetReducer = (
  prevState = initialState,
  action: SelectMeetActionTypes
): any => {
  switch (action.type) {
    case SELECT_MEET:
      return action.payload;

    default:
      return {
        ...prevState,
      };
  }
};
