import {
  FavoriteActionTypes,
  ADD_FAVORITE,
  REMOVE_FAVORITE,
} from "../actions/favorite.action";

const initialState: any = {};

export const favoriteReducer = (
  prevState = initialState,
  action: FavoriteActionTypes
): any => {
  switch (action.type) {
    case ADD_FAVORITE:
      return {
        ...prevState,
        [action.payload._id]: true,
      };

    case REMOVE_FAVORITE:
      return {
        ...prevState,
        [action.payload._id]: false,
      };

    default:
      return {
        ...prevState,
      };
  }
};
