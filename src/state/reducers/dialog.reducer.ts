import {
  CLOSE_DIALOG,
  DialogActionTypes,
  OPEN_DIALOG,
} from "../actions/dialog.action";

const initialState: any = {};

export const dialogReducer = (
  prevState = initialState,
  action: DialogActionTypes
): any => {
  switch (action.type) {
    case CLOSE_DIALOG:
    case OPEN_DIALOG:
      return {
        ...prevState,
        ...action.payload,
      };

    default:
      return {
        ...prevState,
      };
  }
};
