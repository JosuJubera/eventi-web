import {
  SET_TOKEN,
  tokenActionTypes,
  UNSET_TOKEN,
} from "../actions/token.action";

const initialState: string = "";

export const tokenReducer = (
  prevState: string = initialState,
  action: tokenActionTypes
): any => {
  switch (action.type) {
    case SET_TOKEN:
    case UNSET_TOKEN:
      return action.payload;

    default:
      return prevState;
  }
};
