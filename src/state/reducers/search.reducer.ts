import Moment from "moment";

import {
  SEARCH,
  SearchActionTypes,
  SEARCH_RESET,
} from "../actions/search.action";

const initialState: any = {
  city: "",
  endDate: Moment().add(7, "d").endOf("day").valueOf(),
  favorites: false,
  id: "",
  owner: false,
  startDate: Moment().startOf("day").valueOf(),
};

export const searchReducer = (
  prevState = initialState,
  action: SearchActionTypes
): any => {
  switch (action.type) {
    case SEARCH:
      return action.payload;

    case SEARCH_RESET:
      return initialState;

    default:
      return {
        ...prevState,
      };
  }
};
