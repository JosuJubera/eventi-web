import { SET_USER, userActionTypes, UNSET_USER } from "../actions/user.action";
import { User } from "../../core/domain/user/user";

const initialState: any = {};

export const userReducer = (
  prevState: User = initialState,
  action: userActionTypes
): any => {
  switch (action.type) {
    case SET_USER:
      return action.payload;

    case UNSET_USER:
      return action.payload;

    default:
      return prevState;
  }
};
