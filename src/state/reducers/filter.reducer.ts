import { FILTER, FilterActionTypes } from "../actions/filter.action";

const initialState: string = "";

export const filterReducer = (
  _prevState: string,
  action: FilterActionTypes
): any => {
  switch (action.type) {
    case FILTER:
      return action.payload;

    default:
      return initialState;
  }
};
