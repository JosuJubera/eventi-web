import {
  CLOSE_DRAWER,
  DrawerActionTypes,
  OPEN_DRAWER,
} from "../actions/drawer.action";

const initialState: boolean = false;

export const drawerReducer = (
  prevState: boolean = initialState,
  action: DrawerActionTypes
): any => {
  switch (action.type) {
    case CLOSE_DRAWER:
    case OPEN_DRAWER:
      return action.payload;

    default:
      return prevState;
  }
};
