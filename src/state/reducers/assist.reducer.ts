import {
  AssistActionTypes,
  ADD_ASSIST,
  REMOVE_ASSIST,
} from "../actions/assist.action";

const initialState: any = {};

export const assistReducer = (
  prevState = initialState,
  action: AssistActionTypes
): any => {
  switch (action.type) {
    case ADD_ASSIST:
      return {
        ...prevState,
        [action.payload._id]: true,
      };

    case REMOVE_ASSIST:
      return {
        ...prevState,
        [action.payload._id]: false,
      };

    default:
      return {
        ...prevState,
      };
  }
};
