export const LOGIN = "[Login Component] Login";

interface LoginAction {
  type: typeof LOGIN;
  payload: {
    [key: string]: true;
  };
}

export type DialogActionTypes = LoginAction;
