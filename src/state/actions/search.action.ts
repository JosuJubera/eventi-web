export const SEARCH = "[Search Component] Search";
export const SEARCH_RESET = "[Search Component] Search reset";

interface SearchAction {
  type: typeof SEARCH;
  payload: any;
}

interface SearchResetAction {
  type: typeof SEARCH_RESET;
  payload: undefined;
}

export type SearchActionTypes = SearchAction | SearchResetAction;

export const search = (ob: any): SearchActionTypes => {
  return {
    type: SEARCH,
    payload: ob,
  };
};

export const searchReset = (): SearchActionTypes => {
  return {
    type: SEARCH_RESET,
    payload: undefined,
  };
};
