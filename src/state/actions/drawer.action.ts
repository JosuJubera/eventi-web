export const CLOSE_DRAWER = "[Header Component] Close drawer";
export const OPEN_DRAWER = "[Header Component] Open drawer";

interface CloseDrawerAction {
  type: typeof CLOSE_DRAWER;
  payload: false;
}

interface OpenDrawerAction {
  type: typeof OPEN_DRAWER;
  payload: true;
}

export type DrawerActionTypes = CloseDrawerAction | OpenDrawerAction;

export const closeDrawer = (): DrawerActionTypes => {
  return {
    type: CLOSE_DRAWER,
    payload: false,
  };
};

export const openDrawer = (): DrawerActionTypes => {
  return {
    type: OPEN_DRAWER,
    payload: true,
  };
};
