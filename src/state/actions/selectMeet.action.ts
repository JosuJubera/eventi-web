export const SELECT_MEET = "[SELECT_MEET Component] SelectMeet";

interface SelectMeetAction {
  type: typeof SELECT_MEET;
  payload: any;
}

export type SelectMeetActionTypes = SelectMeetAction;

export const selectMeet = (ob: any): SelectMeetActionTypes => {
  return {
    type: SELECT_MEET,
    payload: ob,
  };
};
