export const ADD_FAVORITE = "[Event Component] Add favorite";
export const REMOVE_FAVORITE = "[Event Component] Remove favorite";

interface AddFavoriteAction {
  type: typeof ADD_FAVORITE;
  payload: any;
}

interface RemoveFavoriteAction {
  type: typeof REMOVE_FAVORITE;
  payload: any;
}

export type FavoriteActionTypes = AddFavoriteAction | RemoveFavoriteAction;

export const addFavorite = (event?: any): FavoriteActionTypes => {
  return {
    type: ADD_FAVORITE,
    payload: event,
  };
};

export const removeFavorite = (event?: any): FavoriteActionTypes => {
  return {
    type: REMOVE_FAVORITE,
    payload: event,
  };
};
