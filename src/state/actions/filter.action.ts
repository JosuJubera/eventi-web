export const FILTER = "[Filter Component] Filter";

interface FilterAction {
  type: typeof FILTER;
  payload: any;
}

export type FilterActionTypes = FilterAction;

export const filter = (str: string): FilterActionTypes => {
  return {
    type: FILTER,
    payload: str,
  };
};
