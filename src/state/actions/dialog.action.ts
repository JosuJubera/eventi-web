export const OPEN_DIALOG = "[Event details Component] Open dialog";
export const CLOSE_DIALOG = "[Event details Component] Close dialog";

interface CloseDialogAction {
  type: typeof CLOSE_DIALOG;
  payload: {
    [key: string]: false;
  };
}

interface OpenDialogAction {
  type: typeof OPEN_DIALOG;
  payload: {
    [key: string]: string;
  };
}

export type DialogActionTypes = CloseDialogAction | OpenDialogAction;

export const closeDialog = (type: string): DialogActionTypes => {
  return {
    type: CLOSE_DIALOG,
    payload: {
      [type]: false,
    },
  };
};

export const openDialog = (type: string, id: string): DialogActionTypes => {
  return {
    type: OPEN_DIALOG,
    payload: {
      [type]: id,
    },
  };
};
