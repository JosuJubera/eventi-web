export const ADD_ASSIST = "[Meet list item Component] Add assist";
export const REMOVE_ASSIST = "[Meet list item Component] Remove assist";

interface AddAssistAction {
  type: typeof ADD_ASSIST;
  payload: any;
}

interface RemoveAssistAction {
  type: typeof REMOVE_ASSIST;
  payload: any;
}

export type AssistActionTypes = AddAssistAction | RemoveAssistAction;

export const addAssist = (meet?: any): AssistActionTypes => {
  return {
    type: ADD_ASSIST,
    payload: meet,
  };
};

export const removeAssist = (meet?: any): AssistActionTypes => {
  return {
    type: REMOVE_ASSIST,
    payload: meet,
  };
};
