export const SET_TOKEN = "[App Component] token";
export const UNSET_TOKEN = "[App Component] token";

interface tokenAction {
  type: typeof SET_TOKEN;
  payload: string;
}

interface tokenAction {
  type: typeof UNSET_TOKEN;
  payload: string;
}

export type tokenActionTypes = tokenAction;

export const setToken = (token: string): tokenActionTypes => {
  return {
    type: SET_TOKEN,
    payload: token,
  };
};

export const unsetToken = (): tokenActionTypes => {
  return {
    type: UNSET_TOKEN,
    payload: "",
  };
};
