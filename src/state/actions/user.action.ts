import { User } from "../../core/domain/user/user";

export const SET_USER = "[App Component] user";
export const UNSET_USER = "[App Component] user";

interface setUserAction {
  type: typeof SET_USER;
  payload: undefined | User;
}

interface unsetUserAction {
  type: typeof UNSET_USER;
  payload: undefined;
}

export type userActionTypes = setUserAction | unsetUserAction;

export const setUser = (user: User): userActionTypes => {
  return {
    type: SET_USER,
    payload: user,
  };
};

export const unsetUser = (): userActionTypes => {
  return {
    type: UNSET_USER,
    payload: undefined,
  };
};
