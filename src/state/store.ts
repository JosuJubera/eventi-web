import { createStore, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

import {
  assistReducer,
  dialogReducer,
  drawerReducer,
  favoriteReducer,
  filterReducer,
  searchReducer,
  selectMeetReducer,
  tokenReducer,
  userReducer,
} from "./reducers";

const combinedReducers = combineReducers({
  assists: assistReducer,
  dialogOpened: dialogReducer,
  drawerOpened: drawerReducer,
  favorites: favoriteReducer,
  filter: filterReducer,
  search: searchReducer,
  selectMeet: selectMeetReducer,
  token: tokenReducer,
  user: userReducer,
});

const store = createStore(combinedReducers, composeWithDevTools());

export default store;
