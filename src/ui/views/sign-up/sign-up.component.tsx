import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React, { useContext } from "react";

import styles from "./sign-up.module.scss";
import { UserForm } from "../../components/user-form";
import { signup } from "../../../core/services/auth/signup";
import { TokenContext } from "../../../core/services/auth/token";
import { useHistory } from "react-router-dom";

export const SignUpComponent = (): JSX.Element => {
  const { setToken } = useContext(TokenContext);
  const history = useHistory();
  const handleSubmit = (values: any): void => {
    signup(values)
      .then((response) => {
        setToken(response.data.token);
        history.push("/");
      })
      .catch(() => setToken(""));
  };

  return (
    <div className={styles.container}>
      <h1> Create account </h1>

      <Card className={styles.card}>
        <Container maxWidth="lg">
          <UserForm submit={handleSubmit} />
        </Container>
      </Card>
    </div>
  );
};
