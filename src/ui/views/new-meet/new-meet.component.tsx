import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React from "react";

import styles from "./new-meet.module.scss";
import { MeetFormComponent } from "../../components/meet-form";
import { useLocation, useHistory } from "react-router-dom";
import { createMeet } from "../../../core/services/meets/meets";

export const NewMeetComponent = (): JSX.Element => {
  const location: any = useLocation();
  const history: any = useHistory();

  const eventId = location.state.eventId;

  const handleSubmit = (values: any) => {
    const meet = values;
    meet.eventId = eventId;
    createMeet(meet).then(() => {
      history.goBack();
    });
  };

  return (
    <div className={styles.container}>
      <h1> Create meet </h1>

      <Card className={styles.card}>
        <Container maxWidth="lg">
          <MeetFormComponent submit={handleSubmit} />
        </Container>
      </Card>
    </div>
  );
};
