import { Link } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";

import styles from "./not-found.module.scss";

export const NotFoundComponent = (): JSX.Element => {
  const history = useHistory();

  const goHome = () => {
    history.push("/");
  };

  return (
    <div className={styles.container}>
      <div className={`${styles.column} ${styles.content}`}>
        <div className={styles.code}>404</div>

        <div className={styles.message}>
          Sorry but we could not find the page you are looking for
        </div>

        <div className={styles.column}>
          <Link className={styles.back} component="button" onClick={goHome}>
            Go home
          </Link>
        </div>
      </div>
    </div>
  );
};
