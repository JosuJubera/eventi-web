import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { ThemeProvider } from 'styled-components';
import { generateHawkinsThemeForApp } from '@sfui/client';

import { SignUp } from '../index';

const theme = generateHawkinsThemeForApp({ main: '#f7f7f7', light: '#f7f7f7', dark: '#f7f7f7' });

describe('Component: SignUp', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(
      <ThemeProvider theme={theme}>
        <SignUp />
      </ThemeProvider>,
    );
  });

  it('should render...', () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper).toMatchSnapshot();
  });  
});
