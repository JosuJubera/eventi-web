import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React, { PureComponent } from "react";
import { withRouter } from "react-router";

import styles from "./profile.module.scss";
import { User } from "../../../core/domain/user/user";
import { UserForm } from "../../components/user-form";
import { fetchUser } from "../../../core/services/user/user";

export interface ProfileComponentProps {
  token?: string;
  match: any;
  location: any;
  history: any;
}

export interface ProfileComponentState {
  user?: User;
}

export class ProfileComponent extends PureComponent<
  ProfileComponentProps,
  ProfileComponentState
> {
  constructor(props: ProfileComponentProps) {
    super(props);
    this.state = { user: undefined };
  }

  componentDidMount(): void {
    fetchUser()
      .then((response) => {
        this.setState({ user: response.data });
      })
      .catch(() => {});
  }

  render(): JSX.Element {
    return (
      <div className={styles.container}>
        <h1> Modify account </h1>

        <Card className={styles.card}>
          <Container maxWidth="lg">
            {this.state.user ? <UserForm user={this.state.user} /> : ""}
          </Container>
        </Card>
      </div>
    );
  }
}

export const ProfileComponentWithRouter = withRouter(ProfileComponent);
