import React from "react";

import { LoginComponent } from "../../components/login";
import { TaglineComponent } from "../../components/tagline";
import styles from "./landing.module.scss";

export const LandingComponent = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <div className={styles.tagline}>
        <TaglineComponent />
      </div>

      <div className={styles.login}>
        <LoginComponent />
      </div>
    </div>
  );
};
