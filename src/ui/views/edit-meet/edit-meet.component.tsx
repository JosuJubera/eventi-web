import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React from "react";

import styles from "./edit-meet.module.scss";
import { MeetFormComponent } from "../../components/meet-form";

export const EditMeetComponent = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <h1> Edit meet </h1>

      <Card className={styles.card}>
        <Container maxWidth="lg">
          <MeetFormComponent />
        </Container>
      </Card>
    </div>
  );
};
