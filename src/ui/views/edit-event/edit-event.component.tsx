import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React from "react";
import { useLocation, useHistory } from "react-router-dom";

import styles from "./edit-event.module.scss";
import { EventFormComponent } from "../../components/event-form";
import { Event, IEvent } from "../../../core/domain/event/event";
import { updateEvent } from "../../../core/services/event/event";

export const EditEventComponent = (): JSX.Element => {
  const location = useLocation();
  const history = useHistory();
  const event = new Event(location.state as IEvent);

  const handleSubmit = (values: any) => {
    updateEvent(values).then(() => {
      history.goBack();
    });
  };

  return (
    <div className={styles.container}>
      <h1> Edit event </h1>

      <Card className={styles.card}>
        <Container maxWidth="lg">
          <EventFormComponent event={event} submit={handleSubmit} />
        </Container>
      </Card>
    </div>
  );
};
