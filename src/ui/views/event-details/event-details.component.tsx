import Grid from "@material-ui/core/Grid";
import React, { useEffect, useState } from "react";

import styles from "./event-details.module.scss";
import { MeetTabsComponent } from "../../components/meet-tabs";
import { useParams } from "react-router-dom";
import { DetailsComponent } from "../../components/details";
import { fetchEvent } from "../../../core/services/event/event";
import { Event } from "../../../core/domain/event/event";
import { DialogContainerComponent } from "../../components/dialog-container";
import { useSelector } from "react-redux";
import { Day } from "../../../core/domain/meet/granularity";

export interface EventDetailsComponentState {
  event?: Event;
}

export function EventDetailsComponent() {
  const meet = useSelector((state: any) => state.selectMeet);
  const { eventId } = useParams();
  const [state, setState] = useState<EventDetailsComponentState>({
    event: undefined,
  });

  useEffect(() => {
    if (!state.event && eventId) {
      fetchEvent(eventId)
        .then((response) => {
          const meets = convertMeetsToDays(response.data.meets);

          const event: any = { ...response.data, meets };

          setState({ ...state, event: event });
        })
        .catch(() => {});
    }
  }, [eventId, state]);

  return (
    <div className={styles.container}>
      <Grid className={styles.grid} container spacing={3}>
        <Grid item md={6} xs={12}>
          <DetailsComponent event={state.event} />
        </Grid>

        <Grid item md={6} xs={12}>
          <MeetTabsComponent days={state.event?.meets} />
        </Grid>
      </Grid>

      <DialogContainerComponent id="details">
        <DetailsComponent meet={{ meet, owner: state?.event?.owner?._id }} />
      </DialogContainerComponent>
    </div>
  );
}

const convertMeetsToDays = (
  meets: any[]
): Array<{ date: number; meets: any[] }> => {
  const days: Array<{ date: number; meets: any[] }> = [];
  meets.forEach((meet) => {
    const index = days.findIndex((f) => toDay(f.date) === toDay(meet.date));
    if (index === -1) {
      days.push({ date: meet.date, meets: [meet] });
    } else {
      days[index].meets = [...days[index].meets, meet];
      days[index].meets.sort((a, b) => a.date - b.date);
    }
  });

  days.sort((a, b) => a.date - b.date);
  return days;
};

const toDay = (date: number) => Math.floor(date / new Day().factor);
