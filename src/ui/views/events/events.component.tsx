import Drawer from "@material-ui/core/Drawer";
import Grid from "@material-ui/core/Grid";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import styles from "./events.module.scss";
import { Event } from "../../../core/domain/event/event";
import { EventComponent } from "../../components/event";
import { SearchComponent } from "../../components/search";
import { closeDrawer } from "../../../state/actions/drawer.action";
import { fetchEvents } from "../../../core/services/events/events";
import { GetEventsParams } from "../../../core/infrastructure/api/events";
import { search } from "../../../state/actions";

export interface EventsComponentState {
  drawer: boolean;
  events: Event[];
}

export const EventsComponent = (): JSX.Element => {
  const drawerOpened = useSelector((state: any) => state.drawerOpened);
  const filter = useSelector((state: any) => state.filter);
  const searchValue = useSelector((state: any) => state.search);
  const dispatch = useDispatch();
  const [state, setState] = useState<EventsComponentState>({
    drawer: false,
    events: [],
  });

  const toggleDrawer = (_open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent
  ) => {
    if (
      event.type === "keydown" &&
      ((event as React.KeyboardEvent).key === "Tab" ||
        (event as React.KeyboardEvent).key === "Shift")
    ) {
      return;
    }

    dispatch(closeDrawer());
  };

  const handleSearch = (values: any) => {
    const { endDate, startDate, ...ob } = values;
    dispatch(search(values));
    getEvents(startDate, endDate, ob);
  };

  const getEvents = (
    startDate: number,
    endDate: number,
    params?: GetEventsParams
  ) => {
    fetchEvents(startDate, endDate, params)
      .then((response) => {
        setState({ ...state, events: response.data });
      })
      .catch(() => {});
  };

  useEffect(() => {
    if (!state.events.length) {
      const { startDate, endDate, ...ob } = searchValue;

      getEvents(startDate, endDate, ob);
    }
  }, []);

  return (
    <div className={styles.container}>
      <Grid container spacing={2}>
        {state.events
          .filter(
            (e) => e?.name?.toLowerCase().indexOf(filter) !== -1 || !filter
          )
          .map((event: any, index: number) =>
            event ? (
              <Grid item key={index} xs={3}>
                <EventComponent event={event} />
              </Grid>
            ) : (
              ""
            )
          )}
      </Grid>

      <Drawer anchor="right" open={drawerOpened} onClose={toggleDrawer(false)}>
        <SearchComponent submit={handleSearch} />
      </Drawer>
    </div>
  );
};
