import Card from "@material-ui/core/Card";
import Container from "@material-ui/core/Container";
import React from "react";

import styles from "./new-event.module.scss";
import { EventFormComponent } from "../../components/event-form";
import { createEvent } from "../../../core/services/events/events";
import { useHistory } from "react-router-dom";
import { Event } from "../../../core/domain/event/event";

export const NewEventComponent = (): JSX.Element => {
  const history = useHistory();

  const handleSubmit = (event: Event) => {
    createEvent(event)
      .then((e) => history.push(`${e.data._id}`))
      .catch(() => {});
  };

  return (
    <div className={styles.container}>
      <h1> Create event </h1>

      <Card className={styles.card}>
        <Container maxWidth="lg">
          <EventFormComponent submit={handleSubmit} />
        </Container>
      </Card>
    </div>
  );
};
