import React from "react";
import { useFormik } from "formik";

import styles from "./event-access.module.scss";
import { RequiredField } from "../../../core/domain/form/required-field";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import { useDispatch } from "react-redux";
import { closeDialog } from "../../../state/actions/dialog.action";

export interface EventAccessProps {
  submit?: (values: any) => void;
}
export interface EventAccessState {}

export const EventAccessComponent = (props: EventAccessProps): JSX.Element => {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      password: "",
    },
    validate,
    onSubmit: (values) => {
      if (props.submit) {
        props.submit(values);
      }
    },
  });

  const handleClose = () => {
    dispatch(closeDialog('event-access'));
  };

  return (
    <form
      autoComplete="off"
      className={styles.form}
      onSubmit={formik.handleSubmit}
    >
      <Typography color="inherit" component="h1" gutterBottom variant="h3">
        Event password
      </Typography>

      <TextField
        autoComplete="off"
        className={styles.input}
        error={formik.touched.password && !!formik.errors.password}
        helperText={
          formik.touched.password && formik.errors.password
            ? formik.errors.password
            : ""
        }
        id="password"
        InputProps={{
          startAdornment: <InputAdornment position="start">#</InputAdornment>,
        }}
        label="Password"
        name="password"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="password"
        value={formik.values.password}
        variant="outlined"
      />

      <Grid container item spacing={2} xs={12}>
        <Grid item>
          <Button onClick={handleClose} type="button" variant="contained">
            Back
          </Button>
        </Grid>

        <Grid item>
          <Button color="primary" type="submit" variant="contained">
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

const validate = (values: any) => {
  const password = new RequiredField(values.password);

  const errors: any = {
    ...(password?.error && { password: password.error }),
  };

  return errors;
};
