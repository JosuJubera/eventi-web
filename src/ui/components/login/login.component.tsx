import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Link from "@material-ui/core/Link";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import React from "react";

import styles from "./login.module.scss";
import { Email } from "../../../core/domain/form/email";
import { Password } from "../../../core/domain/form/password";
import { signin } from "../../../core/services/auth/signin";
import { useDispatch } from "react-redux";
import { setToken } from "../../../state/actions";

export const LoginComponent = (): JSX.Element => {
  const dispatch = useDispatch();
  const email = localStorage.getItem("email");

  const formik = useFormik({
    initialValues: {
      email: email || "",
      password: "",
      remember: email ? true : false,
    },
    validate,
    onSubmit: (values) => {
      if (formik.values.remember) {
        localStorage.setItem("email", formik.values.email);
      } else {
        localStorage.removeItem("email");
      }

      signin(values.email, values.password)
        .then((response) => {
          dispatch(setToken(response.data.token));
          localStorage.setItem("token", response.data.token);
        })
        .catch(() => dispatch(setToken("")));
    },
  });

  return (
    <div className={styles.container}>
      <span className={styles.typography}> LOGIN TO YOUR ACCOUNT </span>

      <form
        autoComplete="off"
        className={`${styles.form} ${styles.column}`}
        noValidate
        onSubmit={formik.handleSubmit}
      >
        <div className={styles.column}>
          <TextField
            className={styles.input}
            error={formik.touched.email && !!formik.errors.email}
            helperText={
              formik.touched.email && formik.errors.email
                ? formik.errors.email
                : ""
            }
            id="email"
            label="Email"
            name="email"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            type="email"
            value={formik.values.email}
            variant="outlined"
          />

          <TextField
            className={styles.input}
            error={formik.touched.password && !!formik.errors.password}
            helperText={
              formik.touched.password && formik.errors.password
                ? formik.errors.password
                : ""
            }
            id="password"
            label="Password"
            name="password"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            type="password"
            value={formik.values.password}
            variant="outlined"
          />
        </div>

        <div>
          <FormControlLabel
            control={
              <Checkbox
                checked={formik.values.remember}
                name="remember"
                onChange={formik.handleChange}
              />
            }
            label="Remember me"
          />
        </div>

        <Button
          className={styles.loginButton}
          type="submit"
          variant="contained"
        >
          LOGIN
        </Button>

        <div className={styles.separator}>
          <span
            className={`${styles.text} ${styles.separator__text_before} ${styles.separator__text_after}`}
          >
            OR
          </span>
        </div>

        <Button
          variant="contained"
          className={styles.googleButton}
          href="http://35.224.198.148.xip.io:3001/api/signup/google"
        >
          Log in with Google
        </Button>

        <div className={styles.signupContainer}>
          <span className={styles.typography}> Don't have an account? </span>

          <Link className={styles.typography} href="/sign-up">
            Create an account
          </Link>
        </div>
      </form>
    </div>
  );
};

const validate = (values: any) => {
  const email = new Email(values.email);
  const password = new Password(values.password);

  const errors: any = {
    ...(email.error && { email: email.error }),
    ...(password.error && { password: password.error }),
  };

  return errors;
};
