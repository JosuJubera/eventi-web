import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import { makeStyles, Theme, createStyles, fade } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { filter } from "../../../state/actions";
import debounce from "lodash.debounce";

export interface FilterComponentProps {}
export interface FilterComponentState {}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: theme.spacing(2),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(3),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  })
);

export const FilterComponent = (): JSX.Element => {
  const dispatch = useDispatch();
  const styles = useStyles();

  const handleChange = (e: any) => {
    dispatch(filter(e.target.value));
  };

  const debouncedHandleChange = debounce(handleChange, 500);

  return (
    <div className={styles.search}>
      <div className={styles.searchIcon}>
        <SearchIcon />
      </div>

      <InputBase
        classes={{
          root: styles.inputRoot,
          input: styles.inputInput,
        }}
        inputProps={{ "aria-label": "filter" }}
        onChange={(e) => {
          e.persist();
          debouncedHandleChange(e);
        }}
        placeholder="Filter..."
      />
    </div>
  );
};
