import Card from "@material-ui/core/Card";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Moment from "moment";
import React, { ChangeEvent } from "react";

import styles from "./meet-tabs.module.scss";
import { MeetListComponent } from "../../components/meet-list";

export interface MeetTabsComponentProps {
  days?: any[];
}

export const MeetTabsComponent = (
  props: MeetTabsComponentProps
): JSX.Element => {
  const [value, setValue] = React.useState<number>(0);

  const handleChange = (_event: ChangeEvent<{}>, selectedIndex: number) => {
    setValue(selectedIndex);
  };

  return (
    <div className={styles.container}>
      <Tabs aria-label="day tabs" onChange={handleChange} value={value}>
        {props.days &&
          props.days.map((value: any, index: number) => (
            <Tab key={index} label={Moment(value.date).format("DD/MM/YYYY")} />
          ))}
      </Tabs>

      <Card>
        {props.days?.length ? (
          <MeetListComponent
            meets={props.days[value].meets}
          ></MeetListComponent>
        ) : (
          ""
        )}
      </Card>
    </div>
  );
};
