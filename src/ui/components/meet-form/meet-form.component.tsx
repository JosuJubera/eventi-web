import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Button from "@material-ui/core/Button";
import EventIcon from "@material-ui/icons/Event";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import Moment from "moment";
import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import styles from "./meet-form.module.scss";
import { Meet } from "../../../core/domain/meet/meet";
import { RequiredField } from "../../../core/domain/form/required-field";
import { UploadComponent } from "../upload";
import {
  granularities,
  Granularity,
  Minute,
} from "../../../core/domain/meet/granularity";
import { updateMeet } from "../../../core/services/meet/meet";

export interface MeetFormComponentProps {
  submit?: (values: any) => void;
}

export const MeetFormComponent = (
  props: MeetFormComponentProps
): JSX.Element => {
  const history = useHistory();
  const location: any = useLocation();
  let date;
  if (location?.state?.date) {
    date = Moment(+location?.state?.date);
  }

  const formik = useFormik({
    initialValues: {
      assistantsLimit: location.state?.assistantsLimit
        ? location.state.assistantsLimit.toString()
        : "",
      date: date ? date.format("YYYY-MM-DDTHH:MM") : "",
      description: location.state?.description || "",
      duration: location.state?.duration / new Minute().factor || "",
      granularity: new Minute().factor,
      ...(location.state?._id && { _id: location.state?._id }),
      location: location.state?.location || "",
      logo: location.state?.logo || "",
      name: location.state?.name || "",
      speaker: location.state?.speaker || "",
      speakerLogo: location.state?.speakerLogo || "",
    },
    validate,
    onSubmit: (values) => {
      const meet = new Meet(values as any);
      if (props.submit) {
        props.submit(meet);
      } else {
        updateMeet(meet).then(() => {
          history.goBack();
        });
      }
    },
  });

  return (
    <form autoComplete="off" onSubmit={formik.handleSubmit}>
      <Grid container item spacing={2} xs={12}>
        <Grid container item md={6} spacing={2} xs={12}>
          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12}>
              <TextField
                className={styles.input}
                error={formik.touched.name && !!formik.errors.name}
                helperText={
                  formik.touched.name && formik.errors.name
                    ? formik.errors.name
                    : ""
                }
                id="name"
                label="Name"
                name="name"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.name}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.date && !!formik.errors.date}
                helperText={
                  formik.touched.date && formik.errors.date
                    ? formik.errors.date
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="date"
                label="Date"
                name="date"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="datetime-local"
                value={formik.values.date}
                variant="outlined"
              />
            </Grid>

            <Grid item md={4} sm={9} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.duration && !!formik.errors.duration}
                helperText={
                  formik.touched.duration && formik.errors.duration
                    ? formik.errors.duration
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="duration"
                label="Duration"
                name="duration"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="number"
                value={formik.values.duration}
                variant="outlined"
              />
            </Grid>

            <Grid item md={2} sm={3} xs={12}>
              <FormControl className={styles.input} variant="outlined">
                <InputLabel htmlFor="granularity">Granularity</InputLabel>
                <Select
                  error={
                    formik.touched.granularity && !!formik.errors.granularity
                  }
                  id="granularity"
                  label="Granularity"
                  name="granularity"
                  native
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.granularity}
                  variant="outlined"
                >
                  {granularities.map(
                    (granularity: Granularity, index: number) => (
                      <option key={index} value={granularity.factor}>
                        {granularity.id}
                      </option>
                    )
                  )}
                </Select>
                <FormHelperText>
                  {formik.touched.granularity && formik.errors.granularity
                    ? formik.errors.granularity
                    : ""}
                </FormHelperText>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.location && !!formik.errors.location}
                helperText={
                  formik.touched.location && formik.errors.location
                    ? formik.errors.location
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="location"
                label="Location"
                name="location"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.location}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={
                  formik.touched.assistantsLimit &&
                  !!formik.errors.assistantsLimit
                }
                helperText={
                  formik.touched.assistantsLimit &&
                  formik.errors.assistantsLimit
                    ? formik.errors.assistantsLimit
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="assistantsLimit"
                label="Assistants limit"
                inputProps={{ min: "1" }}
                name="assistantsLimit"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="number"
                value={formik.values.assistantsLimit}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12}>
              <TextField
                className={styles.input}
                error={
                  formik.touched.description && !!formik.errors.description
                }
                helperText={
                  formik.touched.description && formik.errors.description
                    ? formik.errors.description
                    : ""
                }
                id="description"
                label="Description"
                multiline
                name="description"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                rows={4}
                type="text"
                value={formik.values.description}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </Grid>

        <Grid container item md={6} spacing={2} xs={12}>
          <Grid container item spacing={2} xs={12}>
            <Grid item sm={6} xs={12}>
              <UploadComponent
                imageSrc={formik.values.logo}
                field="logo"
                onChange={(imageStr) => formik.setFieldValue("logo", imageStr)}
              >
                <EventIcon className={styles.defaultImage} color="primary" />
              </UploadComponent>
            </Grid>

            <Grid item sm={6} xs={12}>
              <UploadComponent
                imageSrc={formik.values.speakerLogo}
                field="speakerLogo"
                onChange={(imageStr) =>
                  formik.setFieldValue("speakerLogo", imageStr)
                }
                rounded={true}
              >
                <AccountCircleIcon
                  className={styles.defaultImage}
                  color="primary"
                />
              </UploadComponent>
            </Grid>
          </Grid>

          <Grid item md={6} xs={12} />

          <Grid item md={6} xs={12}>
            <TextField
              className={styles.input}
              error={formik.touched.speaker && !!formik.errors.speaker}
              helperText={
                formik.touched.speaker && formik.errors.speaker
                  ? formik.errors.speaker
                  : ""
              }
              id="speaker"
              label="Speaker"
              name="speaker"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              type="text"
              value={formik.values.speaker}
              variant="outlined"
            />
          </Grid>
        </Grid>

        <Grid container item spacing={2} xs={12}>
          <Grid item>
            <Button onClick={history.goBack} type="button" variant="contained">
              Back
            </Button>
          </Grid>

          <Grid item>
            <Button color="primary" type="submit" variant="contained">
              Submit
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

const validate = (values: any): any => {
  const date = new RequiredField(values.date);
  const description = new RequiredField(values.description);
  const duration = new RequiredField(values.duration);
  const granularity = new RequiredField(values.granularity);
  const location = new RequiredField(values.location);
  const name = new RequiredField(values.name);

  const errors: any = {
    ...(date.error && { date: date.error }),
    ...(description.error && { description: description.error }),
    ...(duration.error && { duration: duration.error }),
    ...(granularity.error && { granularity: granularity.error }),
    ...(location.error && { location: location.error }),
    ...(name.error && { name: name.error }),
  };

  return errors;
};
