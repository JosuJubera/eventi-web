import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { useFormik } from "formik";
import React from "react";

import styles from "./authorization.module.scss";
import { Email } from "../../../core/domain/form/email";
import { Password } from "../../../core/domain/form/password";

export const AuthorizationComponent = (props: any): JSX.Element => {
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validate,
    onSubmit: (values) => {
      if (props.submit) {
        props.submit(values);
      }
    },
  });
  return (
    <div className={styles.container}>
      <Typography variant="h5" component="h1">
        Authorization
      </Typography>

      <form
        autoComplete="off"
        className={`${styles.form} ${styles.column}`}
        noValidate
        onSubmit={formik.handleSubmit}
      >
        <div className={styles.column}>
          <TextField
            className={styles.input}
            error={formik.touched.email && !!formik.errors.email}
            helperText={
              formik.touched.email && formik.errors.email
                ? formik.errors.email
                : ""
            }
            id="email"
            label="Email"
            name="email"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            type="email"
            value={formik.values.email}
            variant="outlined"
          />

          <TextField
            className={styles.input}
            error={formik.touched.password && !!formik.errors.password}
            helperText={
              formik.touched.password && formik.errors.password
                ? formik.errors.password
                : ""
            }
            id="password"
            label="Password"
            name="password"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            type="password"
            value={formik.values.password}
            variant="outlined"
          />
        </div>

        <Button
          className={styles.loginButton}
          type="submit"
          variant="contained"
        >
          Authorize
        </Button>
      </form>
    </div>
  );
};

const validate = (values: any) => {
  const email = new Email(values.email);
  const password = new Password(values.password);

  const errors: any = {
    ...(email.error && { email: email.error }),
    ...(password.error && { password: password.error }),
  };

  return errors;
};
