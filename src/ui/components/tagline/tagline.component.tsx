import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import React from "react";

import styles from "./tagline.module.scss";

export const TaglineComponent = (): JSX.Element => {
  return (
    <div className={styles.container}>
      <div className={styles.overlay} />

      <Grid container item md={7} xs={9}>
        <div className={styles.content}>
          <Typography color="inherit" component="h1" gutterBottom variant="h3">
            {"EVENTI"}
          </Typography>

          <Typography
            className={styles.description}
            color="inherit"
            paragraph
            variant="h5"
          >
            {`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
            standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make 
            a type specimen book. It has survived not only five centuries,`}
          </Typography>
        </div>
      </Grid>
    </div>
  );
};
