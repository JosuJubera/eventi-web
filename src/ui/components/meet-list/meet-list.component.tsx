import Divider from "@material-ui/core/Divider";
import React from "react";

import { MeetListItemComponent } from "../meet-list-item";

export interface MeetListComponentProps {
  meets?: any[];
}

export const MeetListComponent = (
  props: MeetListComponentProps
): JSX.Element => {
  return (
    <>
      {props.meets &&
        props.meets.map((value: any, index: number) => (
          <div key={index}>
            <MeetListItemComponent meet={value} />

            <Divider variant="middle" />
          </div>
        ))}
    </>
  );
};
