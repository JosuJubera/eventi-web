import Button from "@material-ui/core/Button";
import React, { ReactNode, useState } from "react";

import styles from "./upload.module.scss";

export interface UploadComponentProps {
  children: ReactNode;
  field: string;
  imageSrc?: string;
  rounded?: boolean;
  onChange: (imageSrc: string) => void;
}
export interface UploadComponentState {
  imageSrc?: string;
}

export const UploadComponent = (props: UploadComponentProps) => {
  const [state, setState]: [any, any] = useState<UploadComponentState>({
    imageSrc: props?.imageSrc,
  });

  return (
    <div className={styles.container}>
      <div className={styles.imageContainer}>
        {state.imageSrc ? (
          <img
            alt="Uploaded"
            className={`${styles.image} ${props.rounded ? styles.rounded : ""}`}
            src={state.imageSrc}
          />
        ) : (
          props.children
        )}
      </div>

      <input
        accept="image/*"
        className={styles.input}
        id={props.field}
        onChange={(e) => {
          if (e.target.files?.length) {
            let reader = new FileReader();
            let file = e.target.files[0];

            reader.onloadend = () => {
              setState({ ...state, imageSrc: reader.result });
              if (reader.result) {
                props.onChange(reader.result.toString());
              }
            };

            reader.readAsDataURL(file);
          }
        }}
        type="file"
      />
      <label htmlFor={props.field}>
        <Button component="span" variant="contained">
          Upload
        </Button>
      </label>
    </div>
  );
};
