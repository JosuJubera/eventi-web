import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Moment from "moment";
import React from "react";
import { useFormik } from "formik";

import styles from "./search.module.scss";
import { useSelector, useDispatch } from "react-redux";
import Link from "@material-ui/core/Link";
import { searchReset } from "../../../state/actions/search.action";
import { RequiredField } from "../../../core/domain/form/required-field";

export interface SearchProps {
  submit?: (values: any) => void;
}

export const SearchComponent = (props: SearchProps): JSX.Element => {
  const dispatch = useDispatch();
  const search = useSelector((state: any) => state.search);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      city: search.city ? search.city : "",
      endDate: search.endDate
        ? Moment(search.endDate).format("YYYY-MM-DD")
        : Moment().add(7, "d").endOf("day").format("YYYY-MM-DD"),
      favorites: search.favorites ? true : false,
      id: search.id ? search.id : "",
      owner: search.owner ? true : false,
      startDate: search.startDate
        ? Moment(search.startDate).format("YYYY-MM-DD")
        : Moment().startOf("day").format("YYYY-MM-DD"),
    },
    validate,
    onSubmit: (values) => {
      if (props && props.submit) {
        const startDate = Moment(values.startDate).valueOf();
        const endDate = Moment(values.endDate).endOf("day").valueOf();

        props.submit({
          city: values.city,
          endDate,
          favorites: values.favorites,
          id: values.id,
          owner: values.owner,
          startDate,
        });
      }
    },
  });

  const handleReset = () => {
    formik.resetForm();
    dispatch(searchReset());
  };

  return (
    <div className={styles.container}>
      <Typography color="inherit" component="h5" gutterBottom variant="h5">
        {"Search"}
      </Typography>

      <Link component="button" variant="body2" onClick={handleReset}>
        Reset
      </Link>

      <form className={styles.form} noValidate onSubmit={formik.handleSubmit}>
        <TextField
          autoComplete="off"
          className={styles.input}
          error={formik.touched.startDate && !!formik.errors.startDate}
          helperText={
            formik.touched.startDate && formik.errors.startDate
              ? formik.errors.startDate
              : ""
          }
          InputLabelProps={{ shrink: true }}
          id="startDate"
          label="StartDate"
          name="startDate"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          type="date"
          value={formik.values.startDate}
          variant="outlined"
        />

        <TextField
          autoComplete="off"
          className={styles.input}
          error={formik.touched.endDate && !!formik.errors.endDate}
          helperText={
            formik.touched.endDate && formik.errors.endDate
              ? formik.errors.endDate
              : ""
          }
          InputLabelProps={{ shrink: true }}
          id="endDate"
          label="EndDate"
          name="endDate"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          type="date"
          value={formik.values.endDate}
          variant="outlined"
        />

        <TextField
          autoComplete="off"
          className={styles.input}
          error={formik.touched.city && !!formik.errors.city}
          helperText={
            formik.touched.city && formik.errors.city ? formik.errors.city : ""
          }
          id="city"
          label="City"
          name="city"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          type="text"
          value={formik.values.city}
          variant="outlined"
        />

        <TextField
          className={styles.input}
          error={formik.touched.id && !!formik.errors.id}
          helperText={
            formik.touched.id && formik.errors.id ? formik.errors.id : ""
          }
          id="id"
          label="Id"
          name="id"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          type="text"
          value={formik.values.id}
          variant="outlined"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={formik.values.favorites}
              id="favorites"
              name="favorites"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
          }
          label="Favorites"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={formik.values.owner}
              id="owner"
              name="owner"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
          }
          label="Own"
        />

        <Button color="primary" type="submit" variant="contained">
          Find event
        </Button>
      </form>
    </div>
  );
};

const validate = (values: any) => {
  const endDate = new RequiredField(values.endDate);
  const startDate = new RequiredField(values.startDate);

  const errors: any = {
    ...(endDate.error && { endDate: endDate.error }),
    ...(startDate.error && { startDate: startDate.error }),
    ...(endDate &&
      startDate &&
      !(startDate.field < endDate.field) && {
        endDate: "End date must be bigger than start",
      }),
  };

  return errors;
};
