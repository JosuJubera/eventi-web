import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import Moment from "moment";
import React from "react";
import { useHistory } from "react-router-dom";

import styles from "./user-form.module.scss";
import { Email } from "../../../core/domain/form/email";
import { Password } from "../../../core/domain/form/password";
import { RequiredField } from "../../../core/domain/form/required-field";
import { User } from "../../../core/domain/user/user";
import { countries } from "./countries";
import { updateUser } from "../../../core/services/user/user";

export interface UserFormProps {
  user?: User;
  submit?: (values: any) => void;
}

export const UserForm = (props: UserFormProps): JSX.Element => {
  const history = useHistory();

  let birthday;
  if (props?.user?.birthday) {
    birthday = Moment(+props?.user?.birthday);
  }

  const formik = useFormik({
    initialValues: {
      birthday: birthday ? birthday.format("YYYY-MM-DD") : "",
      confirmPassword: "",
      country: props?.user?.country || "",
      email: props?.user?.email || "",
      ...(props?.user?._id && { _id: props?.user?._id }),
      name: props?.user?.name || "",
      password: "",
      surname: props?.user?.surname || "",
    },
    validate,
    onSubmit: (values) => {
      const user = new User(values as any);
      if (props.submit) {
        props.submit(user);
      } else {
        updateUser(user).then(() => {
          history.goBack();
        });
      }
    },
  });

  return (
    <div className={styles.container}>
      <form autoComplete="off" onSubmit={formik.handleSubmit}>
        <Grid container item spacing={2} xs={12}>
          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                className={styles.input}
                error={formik.touched.name && !!formik.errors.name}
                helperText={
                  formik.touched.name && formik.errors.name
                    ? formik.errors.name
                    : ""
                }
                id="name"
                label="Name"
                name="name"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.name}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                className={styles.input}
                error={formik.touched.surname && !!formik.errors.surname}
                helperText={
                  formik.touched.surname && formik.errors.surname
                    ? formik.errors.surname
                    : ""
                }
                id="surname"
                label="Surname"
                name="surname"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.surname}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12}>
              <TextField
                className={styles.input}
                error={formik.touched.email && !!formik.errors.email}
                helperText={
                  formik.touched.email && formik.errors.email
                    ? formik.errors.email
                    : ""
                }
                id="email"
                label="Email"
                name="email"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                placeholder="jhon@doe.com"
                type="email"
                value={formik.values.email}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.password && !!formik.errors.password}
                helperText={
                  formik.touched.password && formik.errors.password
                    ? formik.errors.password
                    : ""
                }
                id="password"
                label="Password"
                name="password"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="password"
                value={formik.values.password}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={
                  formik.touched.confirmPassword &&
                  !!formik.errors.confirmPassword
                }
                helperText={
                  formik.touched.confirmPassword &&
                  formik.errors.confirmPassword
                    ? formik.errors.confirmPassword
                    : ""
                }
                id="confirmPassword"
                label="Confirm Password"
                name="confirmPassword"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="password"
                value={formik.values.confirmPassword}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.birthday && !!formik.errors.birthday}
                helperText={
                  formik.touched.birthday && formik.errors.birthday
                    ? formik.errors.birthday
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="birthday"
                label="Birthday"
                name="birthday"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="date"
                value={formik.values.birthday}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <FormControl className={styles.input} variant="outlined">
                <InputLabel htmlFor="country">Country</InputLabel>
                <Select
                  error={formik.touched.country && !!formik.errors.country}
                  id="country"
                  label="Country"
                  name="country"
                  native
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  value={formik.values.country}
                  variant="outlined"
                >
                  <option aria-label="None" value="" />
                  {countries.map((country: string, index: number) => (
                    <option key={index} value={country}>
                      {country}
                    </option>
                  ))}
                </Select>
                <FormHelperText>
                  {formik.touched.country && formik.errors.country
                    ? formik.errors.country
                    : ""}
                </FormHelperText>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container item spacing={2}>
            <Grid item>
              <Button
                onClick={history.goBack}
                type="button"
                variant="contained"
              >
                Back
              </Button>
            </Grid>

            <Grid item>
              <Button color="primary" type="submit" variant="contained">
                Submit
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>

      <Hidden xsDown>
        <AccountCircleIcon color="primary" style={{ fontSize: 240 }} />
      </Hidden>
    </div>
  );
};

const validate = (values: any): any => {
  const birthday = new RequiredField(values.birthday);
  const confirmPassword = new Password(values.confirmPassword);
  const email = new Email(values.email);
  const name = new RequiredField(values.name);
  const password = new Password(values.password);
  const surname = new RequiredField(values.surname);

  const errors: any = {
    ...(birthday.error && { birthday: birthday.error }),
    ...(confirmPassword.error && { confirmPassword: confirmPassword.error }),
    ...(email.error && { email: email.error }),
    ...(name.error && { name: name.error }),
    ...(password.error && { password: password.error }),
    ...(surname.error && { surname: surname.error }),
    ...(password.equalityError(confirmPassword.field) && {
      confirmPassword: password.equalityError(confirmPassword.field),
    }),
  };

  return errors;
};
