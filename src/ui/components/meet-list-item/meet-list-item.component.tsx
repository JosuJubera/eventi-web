import Avatar from "@material-ui/core/Avatar";
import ButtonBase from "@material-ui/core/ButtonBase";
import Chip from "@material-ui/core/Chip";
import Grid from "@material-ui/core/Grid";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import Humanize from "humanize-duration";
import Moment from "moment";
import { useFormik } from "formik";
import React, { useEffect } from "react";

import styles from "./meet-list-item.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { openDialog, addAssist, removeAssist } from "../../../state/actions";
import { selectMeet } from "../../../state/actions/selectMeet.action";
import { assistMeet } from "../../../core/services/meet/meet";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export interface MeetListItemComponentProps {
  checked?: () => void;
  clicked?: () => void;
  meet: {
    assistants: number;
    assistantsLimit?: number | undefined;
    date: number;
    description: string;
    duration: number;
    _id: any;
    location: string;
    logo: string;
    name: string;
    speaker: string;
    speakerLogo: string;
  };
}

export function MeetListItemComponent(props: MeetListItemComponentProps) {
  let assist = useSelector((state: any) => state.assists[props.meet._id]);
  let user = useSelector((state: any) => state.user);
  const dispatch = useDispatch();
  const duration = Humanize(props.meet.duration, {
    largest: 1,
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      assist: assist || false,
    },
    onSubmit: () => {},
  });

  const handleAssist = (e: any) => {
    assistMeet(props.meet._id).then(() => {
      if (assist) {
        dispatch(removeAssist(props.meet));
      } else {
        dispatch(addAssist(props.meet));
      }
    });
  };

  const handleClick = () => {
    dispatch(selectMeet(props.meet));
    dispatch(openDialog("details", props.meet?._id));
  };

  useEffect(() => {
    if (user) {
      assist = user?.assistances?.includes(props.meet._id);
      if (assist) {
        dispatch(addAssist(props.meet));
      } else {
        dispatch(removeAssist(props.meet));
      }
    }
  }, [user]);

  return (
    <Grid container>
      <Grid container item xs={10}>
        <ButtonBase className={styles.container} onClick={handleClick}>
          <Grid item xs={2}>
            <Typography color="textSecondary" align="center">
              {Moment(props.meet.date).format("hh:mm")}
            </Typography>
          </Grid>

          <Grid container item xs={8}>
            <Grid alignItems="flex-start" container direction="column" item xs>
              <Typography>{props.meet.name}</Typography>
              <Grid
                alignItems="center"
                container
                direction="row"
                item
                justify="flex-start"
                spacing={1}
              >
                <Grid item>
                  <Typography color="textSecondary" variant="body2">
                    {duration} | {props.meet.location}
                  </Typography>
                </Grid>

                <Grid item>
                  <Typography noWrap variant="body2">
                    {props.meet.assistants}
                    {props.meet.assistantsLimit
                      ? "/" + props.meet.assistantsLimit
                      : " assistants"}
                  </Typography>
                </Grid>
              </Grid>

              <Chip
                avatar={
                  <Avatar
                    alt={props.meet.speaker}
                    src={props.meet.speakerLogo}
                  />
                }
                label={props.meet.speaker}
              />
            </Grid>
          </Grid>
        </ButtonBase>
      </Grid>

      <Grid className={styles.assist} item xs={2}>
        <FormControlLabel
          control={
            <Switch
              checked={formik.values.assist}
              color="primary"
              inputProps={{ "aria-label": "primary checkbox" }}
              name="assist"
              onChange={handleAssist}
            />
          }
          label=""
        />
      </Grid>
    </Grid>
  );
}
