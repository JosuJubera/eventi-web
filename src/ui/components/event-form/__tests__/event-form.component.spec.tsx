import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { ThemeProvider } from 'styled-components';
import { generateHawkinsThemeForApp } from '@sfui/client';

import { EventFormComponent } from '../index';

const theme = generateHawkinsThemeForApp({ main: '#f7f7f7', light: '#f7f7f7', dark: '#f7f7f7' });

describe('Component: EventFormComponent', () => {
  let wrapper: ShallowWrapper;

  beforeEach(() => {
    wrapper = shallow(
      <ThemeProvider theme={theme}>
        <EventFormComponent />
      </ThemeProvider>,
    );
  });

  it('should render...', () => {
    expect(wrapper.exists()).toBe(true);
    expect(wrapper).toMatchSnapshot();
  });  
});
