import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import EventIcon from "@material-ui/icons/Event";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Moment from "moment";
import React from "react";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";

import styles from "./event-form.module.scss";
import { Event } from "../../../core/domain/event/event";
import { RequiredField } from "../../../core/domain/form/required-field";
import { UploadComponent } from "../upload";

export interface EventFormProps {
  event?: Event;
  submit?: (values: any) => void;
}
export interface EventFormState {
  imageSrc?: string;
}

export const EventFormComponent = (props: EventFormProps): JSX.Element => {
  const history = useHistory();

  let endDate;
  let startDate;
  if (props?.event?.endDate) {
    endDate = Moment(+props?.event?.endDate);
  }

  if (props?.event?.startDate) {
    startDate = Moment(+props?.event?.startDate);
  }

  const formik = useFormik({
    initialValues: {
      city: props.event?.city || "",
      confirmPassword: "",
      description: props.event?.description || "",
      endDate: endDate ? endDate.format("YYYY-MM-DD") : "",
      ...(props.event?._id && { _id: props.event?._id }),
      location: props.event?.location || "",
      logo: props?.event?.logo || "",
      name: props?.event?.name || "",
      owner: props?.event?.owner || "",
      password: props?.event?.password || "",
      private: props?.event?.password ? true : false,
      startDate: startDate ? startDate.format("YYYY-MM-DD") : "",
    },
    validate,
    onSubmit: (values) => {
      if (props.submit) {
        const event = new Event(values as any);
        props.submit(event);
      }
    },
  });

  return (
    <form autoComplete="off" onSubmit={formik.handleSubmit}>
      <Grid container item spacing={2} xs={12}>
        <Grid container item md={9} spacing={2} xs={12}>
          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                className={styles.input}
                error={formik.touched.name && !!formik.errors.name}
                helperText={
                  formik.touched.name && formik.errors.name
                    ? formik.errors.name
                    : ""
                }
                id="name"
                label="Name"
                name="name"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.name}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.startDate && !!formik.errors.startDate}
                helperText={
                  formik.touched.startDate && formik.errors.startDate
                    ? formik.errors.startDate
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="startDate"
                label="StartDate"
                name="startDate"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="date"
                value={formik.values.startDate}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.endDate && !!formik.errors.endDate}
                helperText={
                  formik.touched.endDate && formik.errors.endDate
                    ? formik.errors.endDate
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="endDate"
                label="EndDate"
                name="endDate"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="date"
                value={formik.values.endDate}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.city && !!formik.errors.city}
                helperText={
                  formik.touched.city && formik.errors.city
                    ? formik.errors.city
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="city"
                label="City"
                name="city"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.city}
                variant="outlined"
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <TextField
                autoComplete="off"
                className={styles.input}
                error={formik.touched.location && !!formik.errors.location}
                helperText={
                  formik.touched.location && formik.errors.location
                    ? formik.errors.location
                    : ""
                }
                InputLabelProps={{ shrink: true }}
                id="location"
                label="Location"
                name="location"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                type="text"
                value={formik.values.location}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12}>
              <TextField
                className={styles.input}
                error={
                  formik.touched.description && !!formik.errors.description
                }
                helperText={
                  formik.touched.description && formik.errors.description
                    ? formik.errors.description
                    : ""
                }
                id="description"
                label="Description"
                multiline
                name="description"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                rows={4}
                type="text"
                value={formik.values.description}
                variant="outlined"
              />
            </Grid>
          </Grid>

          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={formik.values.private}
                    id="private"
                    name="private"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                  />
                }
                label="Private"
              />
            </Grid>
          </Grid>

          <div className={styles.input} hidden={!formik.values.private}>
            <Grid
              container
              hidden={!formik.values.private}
              item
              spacing={2}
              xs={12}
            >
              <Grid item md={6} xs={12}>
                <TextField
                  autoComplete="off"
                  className={styles.input}
                  error={formik.touched.password && !!formik.errors.password}
                  helperText={
                    formik.touched.password && formik.errors.password
                      ? formik.errors.password
                      : ""
                  }
                  id="password"
                  label="Password"
                  name="password"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.password}
                  variant="outlined"
                />
              </Grid>

              <Grid item md={6} xs={12}>
                <TextField
                  autoComplete="off"
                  className={styles.input}
                  error={
                    formik.touched.confirmPassword &&
                    !!formik.errors.confirmPassword
                  }
                  helperText={
                    formik.touched.confirmPassword &&
                    formik.errors.confirmPassword
                      ? formik.errors.confirmPassword
                      : ""
                  }
                  id="confirmPassword"
                  label="Confirm Password"
                  name="confirmPassword"
                  onBlur={formik.handleBlur}
                  onChange={formik.handleChange}
                  type="password"
                  value={formik.values.confirmPassword}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </div>
        </Grid>

        <Grid container item md={3} spacing={2} xs={12}>
          <Grid item sm={6} xs={12}>
            <UploadComponent
              imageSrc={formik.values.logo}
              field="logo"
              onChange={(imageStr) => formik.setFieldValue("logo", imageStr)}
            >
              <EventIcon className={styles.defaultImage} color="primary" />
            </UploadComponent>
          </Grid>
        </Grid>

        <Grid container item spacing={2} xs={12}>
          <Grid item>
            <Button onClick={history.goBack} type="button" variant="contained">
              Back
            </Button>
          </Grid>

          <Grid item>
            <Button color="primary" type="submit" variant="contained">
              Submit
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

const validate = (values: any) => {
  const city = new RequiredField(values.city);
  const description = new RequiredField(values.description);
  const endDate = new RequiredField(values.endDate);
  const name = new RequiredField(values.name);
  const startDate = new RequiredField(values.startDate);

  let confirmPassword;
  let password;

  if (values.private === true) {
    confirmPassword = new RequiredField(values.confirmPassword);
    password = new RequiredField(values.password);
  }

  const errors: any = {
    ...(city.error && { city: city.error }),
    ...(description.error && { description: description.error }),
    ...(values.private &&
      confirmPassword?.error && { confirmPassword: confirmPassword.error }),
    ...(endDate.error && { endDate: endDate.error }),
    ...(name.error && { name: name.error }),
    ...(values.private && password?.error && { password: password.error }),
    ...(startDate.error && { startDate: startDate.error }),
    ...(values.private &&
      confirmPassword &&
      password &&
      password.equalityError(confirmPassword.field) && {
        confirmPassword: password.equalityError(confirmPassword.field),
      }),
  };

  return errors;
};
