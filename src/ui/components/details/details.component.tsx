import AccessTimeIcon from "@material-ui/icons/AccessTime";
import Avatar from "@material-ui/core/Avatar";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import EventIcon from "@material-ui/icons/Event";
import FingerprintIcon from "@material-ui/icons/Fingerprint";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import LocationCityIcon from "@material-ui/icons/LocationCity";
import PeopleIcon from "@material-ui/icons/People";
import PlaceIcon from "@material-ui/icons/Place";
import TodayIcon from "@material-ui/icons/Today";
import Typography from "@material-ui/core/Typography";
import Moment from "moment";
import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import styles from "./details.module.scss";
import { Event } from "../../../core/domain/event/event";
import { openDialog } from "../../../state/actions";
import { useDispatch, useSelector } from "react-redux";
import { DialogContainerComponent } from "../dialog-container";
import { AuthorizationComponent } from "../authorization";
import { removeEvent } from "../../../core/services/event/event";
import { reauth } from "../../../core/services/user/user";
import { Meet } from "../../../core/domain/meet/meet";
import { removeMeet } from "../../../core/services/meet/meet";

export interface DetailsComponentProps {
  event?: Event;
  meet?: {
    owner?: string;
    meet: Meet;
  };
}

export const DetailsComponent = (props: DetailsComponentProps): JSX.Element => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  let user = useSelector((state: any) => state.user);

  const handleDelete = () => {
    if (props?.event?._id) {
      dispatch(openDialog("authorization", props.event._id));
    }
    if (props?.meet?.meet._id) {
      dispatch(openDialog("authorization", props.meet.meet._id));
    }
  };

  const handleSubmit = (values: any) => {
    reauth(values).then(() => {
      if (props.event) {
        removeEvent(props.event._id).then(() => history.push("/"));
      }
      if (props.meet) {
        removeMeet(props.meet.meet._id).then(() => window.location.reload());
      }
    });
  };

  const handleEdit = () => {
    if (props.event) {
      history.push(`${location.pathname}/edit`, props.event);
      return;
    }

    if (props.meet) {
      history.push(
        `${location.pathname}/meet/${props.meet.meet._id}/edit`,
        props.meet.meet
      );

      return;
    }
  };

  let endDate;
  let startDate;
  if (props.event) {
    endDate = props.event.endDate
      ? Moment(props.event.endDate).format("LL")
      : "";
    startDate = props.event.startDate
      ? Moment(props.event.startDate).format("LL")
      : "";
  }

  let timeStart;
  if (props.meet?.meet) {
    timeStart = props.meet?.meet.date
      ? Moment(props.meet?.meet.date).format("LT")
      : "";
  }

  return (
    <div>
      <Typography color="inherit" component="h1" gutterBottom variant="h3">
        <div className={styles.titleWrapper}>
          <span>
            {props.event?.name} {props.meet?.meet.name}
          </span>

          {props.event?.owner?._id === user?._id ||
          props.meet?.owner === user?._id ? (
            <div>
              <IconButton aria-label="edit" onClick={handleEdit}>
                <EditIcon />
              </IconButton>

              <IconButton aria-label="delete" onClick={handleDelete}>
                <DeleteIcon />
              </IconButton>
            </div>
          ) : (
            ""
          )}
        </div>
      </Typography>

      <Typography color="inherit" paragraph variant="h5">
        {props.event?.description}
        {props.meet?.meet.description}
      </Typography>

      <List>
        {props.event?.password ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <FingerprintIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={"#" + props.event?.password}
              secondary="Password"
            />
          </ListItem>
        ) : (
          ""
        )}

        {timeStart ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <AccessTimeIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={timeStart} secondary="Time start" />
          </ListItem>
        ) : (
          ""
        )}

        {startDate ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <TodayIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={startDate} secondary="Start date" />
          </ListItem>
        ) : (
          ""
        )}

        {endDate ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <EventIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={endDate} secondary="End date" />
          </ListItem>
        ) : (
          ""
        )}

        {props.event?.location || props.meet?.meet.location ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <PlaceIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={props.event?.location || props.meet?.meet.location}
              secondary="Location"
            />
          </ListItem>
        ) : (
          ""
        )}

        {props.event?.city ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <LocationCityIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={props.event?.city} secondary="City" />
          </ListItem>
        ) : (
          ""
        )}

        {props.meet?.meet.speaker ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar
                alt={props.meet.meet.speaker}
                src={props.meet.meet.speakerLogo}
              />
            </ListItemAvatar>
            <ListItemText
              primary={props.meet?.meet.speaker}
              secondary="Speaker"
            />
          </ListItem>
        ) : (
          ""
        )}

        {props.meet?.meet.assistants ? (
          <ListItem>
            <ListItemAvatar>
              <Avatar>
                <PeopleIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={`${props.meet?.meet.assistants}${
                props.meet?.meet.assistantsLimit
                  ? "/" + props.meet?.meet.assistantsLimit
                  : ""
              }`}
              secondary="Assistants"
            />
          </ListItem>
        ) : (
          ""
        )}
      </List>

      <DialogContainerComponent id="authorization">
        <AuthorizationComponent submit={handleSubmit} />
      </DialogContainerComponent>
    </div>
  );
};
