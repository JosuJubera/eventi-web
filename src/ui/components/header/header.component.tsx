import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import AppBar from "@material-ui/core/AppBar";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import FilterListIcon from "@material-ui/icons/FilterList";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import styles from "./header.module.scss";
import { ProfileMenuComponent } from "../profile-menu";
import { FilterComponent } from "../filter";
import { useDispatch, useSelector } from "react-redux";
import { openDrawer } from "../../../state/actions/drawer.action";

export const HeaderComponent = (): JSX.Element => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const user = useSelector((state: any) => state.user);

  const username = user.name ? `${user.name} ${user.surname}` : "";

  let isRootPath = false;
  let isEventPath = false;

  if (location.pathname === "/") {
    isRootPath = true;
  }

  const urlParts = location.pathname.split("/");
  if (urlParts[1] === "event" && !urlParts.includes("new")) {
    isEventPath = true;
  }

  const handleNew = () => {
    if (isEventPath) {
      history.push(`${location.pathname}/meet/new`, { eventId: urlParts[2] });
      return;
    }

    history.push("/event/new");
  };

  const handleBackNavigation = () => {
    if (
      location.pathname.indexOf("edit") !== -1 ||
      location.pathname.indexOf("new") !== -1
    ) {
      history.goBack();
    } else {
      history.push("/");
    }
  };

  const toggleDrawer = () => {
    dispatch(openDrawer());
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          {isRootPath ? (
            ""
          ) : (
            <IconButton
              aria-label="navigate back"
              className={styles.menuButton}
              color="inherit"
              edge="start"
              onClick={handleBackNavigation}
            >
              <ArrowBackIcon />
            </IconButton>
          )}

          <Typography className={styles.title} variant="h6">
            Eventi
          </Typography>

          <div className={styles.grow} />

          {isRootPath ? <FilterComponent /> : ""}

          <ProfileMenuComponent userName={username} />

          {isRootPath || isEventPath ? (
            <IconButton color="inherit" onClick={handleNew}>
              <AddCircleOutlineIcon />
            </IconButton>
          ) : (
            ""
          )}

          {isRootPath ? (
            <IconButton
              aria-label="open drawer"
              color="inherit"
              edge="end"
              onClick={toggleDrawer}
            >
              <FilterListIcon />
            </IconButton>
          ) : (
            ""
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};
