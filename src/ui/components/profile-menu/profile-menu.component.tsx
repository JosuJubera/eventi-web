import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import React from "react";

import styles from "./profile-menu.module.scss";
import { useDispatch } from "react-redux";
import { setToken } from "../../../state/actions";

export interface ProfileMenuComponentProps {
  userName?: string;
}

export const ProfileMenuComponent = (
  props: ProfileMenuComponentProps
): JSX.Element => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleProfile = () => {
    history.push("/profile");
  };

  const handleLogout = () => {
    localStorage.setItem("token", "");
    dispatch(setToken(""));
    history.push("/");
  };

  return (
    <>
      <Button
        aria-controls="profile-menu"
        aria-haspopup="true"
        color="inherit"
        onClick={handleClick}
      >
        <Avatar alt="User" className={styles.avatar} />
        <span className={styles.sectionDesktop}> {props.userName} </span>
      </Button>

      <Menu
        id="profile-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {props.userName ? (
          <MenuItem className={styles.sectionMobile} disabled>
            {props.userName}
          </MenuItem>
        ) : (
          ""
        )}
        <MenuItem onClick={handleProfile}> Profile </MenuItem>
        <MenuItem onClick={handleLogout}> Logout </MenuItem>
      </Menu>
    </>
  );
};
