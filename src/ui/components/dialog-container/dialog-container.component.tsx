import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import React, { ReactNode } from "react";
import { useSelector, useDispatch } from "react-redux";

import styles from "./dialog-container.module.scss";
import { closeDialog } from "../../../state/actions/dialog.action";

export interface DialogContainerComponentProps {
  children: ReactNode;
  id: string;
}

export const DialogContainerComponent = (
  props: DialogContainerComponentProps
): JSX.Element => {
  const dialogState = useSelector((state: any) => state.dialogOpened[props.id]);
  const dispatch = useDispatch();

  const dialogOpened = dialogState ? true : false;

  const handleClose = () => {
    dispatch(closeDialog(props.id));
  };

  return (
    <Dialog
      aria-labelledby="dialog"
      className={styles.container}
      onClose={handleClose}
      open={dialogOpened}
    >
      <DialogContent>{props.children}</DialogContent>
    </Dialog>
  );
};
