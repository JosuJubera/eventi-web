import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import StarIcon from "@material-ui/icons/Star";
import Typography from "@material-ui/core/Typography";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";

import styles from "./event.module.scss";

import DateRangeIcon from "@material-ui/icons/DateRange";
import { useSelector, useDispatch } from "react-redux";
import {
  addFavorite,
  removeFavorite,
  openDialog,
} from "../../../state/actions";
import { DialogContainerComponent } from "../dialog-container";
import { EventAccessComponent } from "../event-access";
import { closeDialog } from "../../../state/actions/dialog.action";
import { favoriteEvent } from "../../../core/services/event/event";
import CardHeader from "@material-ui/core/CardHeader";

export interface EventProps {
  event:
    | {
        description?: string;
        endDate?: number;
        id?: string;
        location?: string;
        logo?: string;
        name?: string;
        owner?: string;
        password?: string;
        startDate?: number;
      }
    | any;
}

export const EventComponent = (props: EventProps): JSX.Element => {
  const dispatch = useDispatch();
  let favorite = useSelector((state: any) => state.favorites[props.event._id]);
  let user = useSelector((state: any) => state.user);
  const history = useHistory();
  let dialogOpened = useSelector(
    (state: any) => state.dialogOpened["event-access"]
  );

  const handleExplore = (id: string) => {
    if (props.event.password) {
      dispatch(openDialog("event-access", props.event._id));
    } else {
      history.push(`/event/${id}`);
    }
  };

  const handleFavorite = () => {
    favoriteEvent(props.event._id).then(() => {
      if (favorite) {
        dispatch(removeFavorite(props.event));
      } else {
        dispatch(addFavorite(props.event));
      }
    });
  };

  const handleSubmit = (values: { password: string }) => {
    if (values.password && values.password === props.event.password) {
      dispatch(closeDialog("event-access"));
      history.push(`/event/${props.event._id}`);
    }
  };

  useEffect(() => {
    if (user) {
      favorite = user?.favorites?.includes(props.event._id);
      if (favorite) {
        dispatch(removeFavorite(props.event));
      } else {
        dispatch(addFavorite(props.event));
      }
    }
  }, [user]);

  return (
    <>
      <Card style={{ width: "100% !important", height: "100% !important" }}>
        <CardHeader
          className={styles.tileBar}
          action={
            <IconButton
              aria-label={`star ${props.event.name}`}
              className={styles.icon}
              onClick={handleFavorite}
            >
              {favorite ? <StarIcon /> : <StarBorderIcon />}
            </IconButton>
          }
        />
        {props.event.logo ? (
          <CardMedia
            alt={props.event.name}
            component="img"
            height="140"
            image={props.event.logo}
            title={props.event.name}
          />
        ) : (
          <div className={styles.emptyImageContainer}>
            <DateRangeIcon color="primary" className={styles.emptyImage} />
          </div>
        )}

        <CardContent>
          <Typography component="h2" gutterBottom noWrap variant="h5">
            {props.event.name}
          </Typography>

          <Typography
            className={styles.trucatedParagraph}
            component="p"
            color="textSecondary"
            variant="body2"
          >
            {props.event.description}
          </Typography>
        </CardContent>

        <CardActions>
          <Button
            color="primary"
            onClick={() => handleExplore(props.event._id)}
            size="small"
          >
            Learn More
          </Button>
        </CardActions>
      </Card>

      {dialogOpened === props.event._id ? (
        <DialogContainerComponent id="event-access">
          <EventAccessComponent submit={handleSubmit} />
        </DialogContainerComponent>
      ) : (
        ""
      )}
      {/* <GridListTileBar
        actionIcon={
          <IconButton
            aria-label={`star ${props.event.name}`}
            className={styles.icon}
            onClick={handleFavorite}
          >
            {favorite ? <StarIcon /> : <StarBorderIcon />}
          </IconButton>
        }
        actionPosition="right"
        className={styles.tileBar}
        titlePosition="top"
      /> */}
    </>
  );
};
